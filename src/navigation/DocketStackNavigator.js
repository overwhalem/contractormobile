import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';

import DocketScreen from '../screens/DocketScreen';
import DocketDetailScreen from '../screens/DocketDetailScreen';
import DocketFormScreen from '../screens/DocketFormScreen';
import DocketFormSignatureScreen from '../screens/DocketFormSignatureScreen';
import DocketFormCameraScreen from '../screens/DocketFormCameraScreen';

const DocketStackNavigator = createStackNavigator(
    {
        Docket: {
            screen: DocketScreen
        },
        DocketDetail: {
            screen: DocketDetailScreen,
        },
        DocketForm: {
            screen: DocketFormScreen,
        },
        DocketFormSignature: {
            screen: DocketFormSignatureScreen,
        },
        DocketFormCamera: {
            screen: DocketFormCameraScreen,
        }
    },
    {
        initialRouteName: "Docket"
    }
);

DocketStackNavigator.navigationOptions = ({ navigation }) => {
    let tabBarVisible = true;
    if (navigation.state.index > 0) {
        tabBarVisible = false;
    }

    return {
        tabBarVisible,
        tabBarLabel: 'Công việc',
        tabBarIcon: ({ focused }) => (
            <TabBarIcon
                focused={focused}
                name={
                    Platform.OS === 'ios'
                        ? `ion-clipboard${focused ? '' : '-outline'}`
                        : 'md-clipboard'
                }
            />
        ),
    };
};

export default DocketStackNavigator;