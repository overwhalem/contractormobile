import React from 'react';
import { createStackNavigator } from 'react-navigation';

import LoginScreen from '../screens/LoginScreen';

export default createStackNavigator({
    Login: LoginScreen,
});