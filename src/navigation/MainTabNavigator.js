import React from 'react';
import { Platform } from 'react-native';
import { createBottomTabNavigator } from 'react-navigation';


import HomeStackNavigator from './HomeStackNavigator';
import DocketStackNavigator from './DocketStackNavigator';
import SettingStackNavigator from './SettingStackNavigator';

import COLORS from '../constants/colors';

const MainTabNavigator = createBottomTabNavigator(
    {
        HomeStackNavigator,
        DocketStackNavigator,
        SettingStackNavigator,
    }, {
        tabBarOptions: {
            activeTintColor: COLORS.tabIconSelected,
        }
    }
);

export default MainTabNavigator;