import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import HomeScreen from '../screens/HomeScreen';

const HomeStackNavigator = createStackNavigator({
    Home: {
        screen: HomeScreen
    },
});

HomeStackNavigator.navigationOptions = {
    tabBarLabel: 'Tổng quan',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? `ios-home${focused ? '' : '-outline'}`
                    : 'md-home'
            }
        />
    ),
};

export default HomeStackNavigator;