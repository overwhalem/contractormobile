import React from 'react';
import { Platform } from 'react-native';
import { createStackNavigator } from 'react-navigation';

import TabBarIcon from '../components/TabBarIcon';
import SettingScreen from '../screens/SettingScreen';

const SettingStackNavigator = createStackNavigator({
    Setting: {
        screen: SettingScreen
    },
});

SettingStackNavigator.navigationOptions = {
    tabBarLabel: 'Tùy chọn',
    tabBarIcon: ({ focused }) => (
        <TabBarIcon
            focused={focused}
            name={
                Platform.OS === 'ios'
                    ? 'ios-options'
                    : 'md-options'
            }
        />
    ),
};

export default SettingStackNavigator;