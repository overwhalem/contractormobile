import { API } from "./index";

export const apiSearchSubcontractor = (searchValue, criteria) => {
    const payload = {
        ...searchValue,
        ...criteria
    };
    const headers = {
        params: {
        }
    };

    return API.post("search", payload, headers);
}