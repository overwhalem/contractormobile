import { API } from "./index";

export const apiGetAllProfession = () => {
    return API.get("/professions");
};