import { API } from "./index";

export const apiGetAllDocketByWorkerId = (workerId) => {
    const payload = null;
    const headers = {
    };

    return API.get(`workers/${workerId}/dockets?randomDate=${new Date()}`);
}


export const apiGetDocketDetailById = (docketId) => {
    const payload = null;
    const headers = {
    };

    return API.get(`dockets/${docketId}?randomDate=${new Date()}`);
}

export const apiSetDocketStatusToWorkingById = (docketId) => {
    const payload = null;
    const headers = {
    };

    return API.post(`dockets/${docketId}/working?randomDate=${new Date()}`);
}

export const apiSetDocketStatusToNeedApproveById = (docketId) => {
    const payload = null;
    const headers = {
    };

    return API.post(`dockets/${docketId}/needapprove?randomDate=${new Date()}`);
}