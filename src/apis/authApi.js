import { API } from "./index";

export const apiLogin = (username, password) => {
    const payload = null;
    const headers = {
        params: {
            username,
            password,
        }
    };
    return API.post(`login?randomDate=${new Date()}`, payload, headers);
}