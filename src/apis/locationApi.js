import { API } from "./index";

export const apiGetAllLocation = () => {
    return API.get("/locations");
};