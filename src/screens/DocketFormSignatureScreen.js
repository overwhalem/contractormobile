import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';
import ExpoPixi from 'expo-pixi';
import { Container, Button, Text } from 'native-base';

import COLORS from '../constants/colors';

import { changeSignature } from '../actions/docketFormAction';

class DocketFormSignatureScreen extends Component {
    static navigationOptions = {
        title: 'Kí tên',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return (
            <Container>
                <View style={styles.signatureCanvasContainer}>
                    <ExpoPixi.Signature
                        ref='signatureCanvas' //Important to be able to call this obj
                        strokeAlpha={0.7} // opacity of the brush
                        style={styles.signatureCanvas}
                    />
                </View>

                <View style={styles.buttonContainer}>
                    <Button block info onPress={this._clearCanvas}>
                        <Text>Clear</Text>
                    </Button>

                    <Button block success onPress={this._savedCanvas} style={{ marginTop: 10 }}>
                        <Text>Sign</Text>
                    </Button>
                </View>
            </Container>
        )
    }

    _clearCanvas = () => {
        this.refs.signatureCanvas.clear()
    }

    _savedCanvas = async () => {
        const signature_result = await
            this.refs.signatureCanvas.takeSnapshotAsync({
                format: "jpeg", // 'png' also supported
                quality: 0.5, // quality 0 for very poor 1 for very good
                result: 'file', // 
                // width: 400,
                // height: 200
            });

        console.log(signature_result);
        this.props.changeSignature(signature_result.uri)
        this.props.navigation.goBack();
        // this.setState({
        //     signature: signature_result.uri
        // });
    }
}

function mapStateToProps(state) {
    return {
    };
}

const styles = StyleSheet.create({
    signatureCanvasContainer: {
        padding: 10,
        height: 400,
        backgroundColor: '#eee',
    },
    buttonContainer: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signatureCanvas: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

export default connect(
    mapStateToProps,
    { changeSignature },
)(DocketFormSignatureScreen);