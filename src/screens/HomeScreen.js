import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
} from 'react-native';
import {
    Container,
} from 'native-base';
import { connect } from 'react-redux';

import { MonoText } from '../components/StyledText';
import COLORS from '../constants/colors';

class HomeScreen extends Component {
    static navigationOptions = {
        title: 'Tổng quan',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return (
            <Container style={styles.container}>
                <ScrollView contentContainerStyle={styles.contentContainer}>
                    <View style={styles.welcomeContainer}>
                        <View style={styles.codeHighlightContainer}>
                            <MonoText style={styles.codeHighlightText}>screens/HomeScreen.js</MonoText>
                        </View>
                    </View>
                </ScrollView>
            </Container>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
});

function mapStateToProps(state) {
    return {
    };
}

export default connect(
    mapStateToProps,
)(HomeScreen);