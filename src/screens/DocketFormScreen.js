import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    ScrollView,
    StyleSheet,
    View,
    TouchableOpacity,
    Image
} from 'react-native';
import {
    Container,
    Button,
    Text,
    Card,
} from 'native-base';


import COLORS from '../constants/colors';

class DocketFormScreen extends Component {
    static navigationOptions = {
        title: 'Hợp đồng công việc',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    renderDocketForm() {
        const { pictureUrl, signatureUrl } = this.props;

        return (
            <ScrollView
                style={styles.docketFormContainer}
                contentContainerStyle={styles.contentContainer}
            >
                <Card style={styles.docketFormCard}>

                    <Text>This is Docket form details</Text>
                    <TouchableOpacity
                        style={styles.signatureContainer}
                        onPress={() => this.props.navigation.navigate('DocketFormSignature')}
                    >
                        {signatureUrl
                            ? <Image
                                resizeMode={'contain'}
                                style={{ width: 150, height: 150 }}
                                source={{ uri: signatureUrl }}
                            />
                            : <Text>Signature</Text>}

                    </TouchableOpacity>

                    <TouchableOpacity
                        style={styles.signatureContainer}
                        onPress={() => this.props.navigation.navigate('DocketFormCamera')}
                    >
                        {pictureUrl
                            ? <Image
                                resizeMode={'contain'}
                                style={{ width: 150, height: 150 }}
                                source={{ uri: pictureUrl }}
                            />
                            : <Text>Picture</Text>}

                    </TouchableOpacity>

                </Card>
            </ScrollView>
        );
    }

    renderUpdateFormButton() {
        return (
            <View style={styles.updateFormButtonContainer}>
                <Button
                    style={styles.updateFormButton}
                    onPress={() => this._onUpdateFormButtonPress()}
                >
                    <Text>LƯU</Text>
                </Button>
            </View>
        );
    };

    render() {
        return (
            <Container style={styles.container}>
                {this.renderDocketForm()}

                {this.renderUpdateFormButton()}
            </Container>
        );
    };

    _onUpdateFormButtonPress = () => {

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    contentContainer: {
        paddingTop: 15,
        paddingLeft: 5,
        paddingRight: 5,
        backgroundColor: '#fafafa'
    },
    docketFormContainer: {
        flex: 0.91,
        height: 600,
    },
    docketFormCard: {
        flex: 1,
    },

    updateFormButtonContainer: {
        flex: 0.09,
        flexDirection: 'row',
        padding: 0,
    },
    updateFormButton: {
        height: '100%',
        flex: 1,
        backgroundColor: COLORS.tintColor,
        justifyContent: 'center',
        alignItems: 'center',
        shadowOffset: { height: 0, width: 0 },
        shadowOpacity: 0,
        elevation: 0
    },

    signatureContainer: {
        width: 150,
        height: 150,
        backgroundColor: '#eee',
        justifyContent: 'center',
        alignItems: 'center',
        color: '#aaa'
    },
});

function mapStateToProps(state) {
    return {
        pictureUrl: state.docketFormReducer.pictureUrl,
        signatureUrl: state.docketFormReducer.signatureUrl,
    };
}

export default connect(
    mapStateToProps,
)(DocketFormScreen);



