import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    ScrollView,
    StyleSheet,
    View,
    TouchableOpacity,
} from 'react-native';
import {
    Container,
    Card,
    CardItem,
    Text,
    Button,
    Icon,
    Body,
    Right
} from 'native-base';

import COLORS from '../constants/colors';
import { DOCKETSTATUSNAME, DOCKETSTATUSID } from '../constants/docketStatus';
import { getAllDocketByWorkerId } from '../actions/docketAction';
import { getDocketDetailById } from '../actions/docketDetailAction';

class DocketScreen extends Component {
    static navigationOptions = {
        title: 'Công việc',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    constructor(props) {
        super(props);
        this.componentDidFocus = this.props.navigation.addListener(
            'didFocus',
            (payload) => {
                this.props.getAllDocketByWorkerId(this.props.workerId);
            }
        );
    }

    renderDocketCardStatusButton(status) {
        return (
            <Button
                rounded
                style={styles.cardStatusButton}
                danger={status === DOCKETSTATUSID.CONFIRMED}
                success={status === DOCKETSTATUSID.WORKING}
                warning={status === DOCKETSTATUSID.NEEDAPPROVE}
                light={status === DOCKETSTATUSID.APPROVED}
            >
                <Text>{DOCKETSTATUSNAME[status]}</Text>
            </Button>
        );
    }

    renderDocketCard() {
        const { docketList } = this.props;

        return docketList.map((docket) => {
            return (
                <Card style={styles.docketCard} key={docket.docketId}>
                    <TouchableOpacity
                        onPress={() => this._onPressDocketCard(docket.docketId)}
                    >
                        <CardItem>
                            <Body style={styles.cardProfession}>
                                <Text style={styles.cardProfessionText}>{docket.profession}</Text>
                            </Body>
                            <Right>
                                {this.renderDocketCardStatusButton(docket.status)}
                            </Right>
                        </CardItem>
                        <CardItem>
                            <Body>
                                <Text>{`${docket.docketDetail}`}</Text>
                            </Body>
                        </CardItem>
                        <CardItem style={styles.cardDateLocation}>
                            <Body style={styles.cardBodyDateLocation}>
                                <Icon
                                    name='md-calendar'
                                    style={styles.cardDateLocationText}
                                />
                                <Text style={styles.cardDateLocationText}>
                                    {`  ${docket.date}   |   `}
                                </Text>
                                <Icon
                                    name='md-locate'
                                    style={styles.cardDateLocationText}
                                />
                                <Text style={styles.cardDateLocationText}>
                                    {`  ${docket.location}`}
                                </Text>
                            </Body>
                        </CardItem>
                    </TouchableOpacity>
                </Card>
            );
        });
    };

    render() {
        return (
            <Container style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {this.renderDocketCard()}
                </ScrollView>
            </Container>
        );
    };

    _onPressDocketCard = (docketId) => {
        const getSuccessCallback = () => {
            this.props.navigation.navigate('DocketDetail');
        }
        this.props.getDocketDetailById(docketId, getSuccessCallback);
    };
}

function mapStateToProps(state) {
    return {
        workerId: state.authReducer.profile.userId,
        docketList: state.docketReducer.docketList
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // marginTop: 15,
        backgroundColor: '#fafafa',
    },
    contentContainer: {
        // flex: 1,
        padding: 15,
        backgroundColor: '#fafafa'
    },
    docketCard: {
        borderRadius: 3,
        marginTop: 10,
    },
    cardProfession: {
        justifyContent: 'center',
        alignItems: 'flex-start',
    },
    cardProfessionText: {
        fontSize: 22,
        fontWeight: 'bold',
    },
    cardStatusButton: {
        width: '85%',
        height: 25,
        justifyContent: 'center',
        alignItems: 'center',
    },
    cardDateLocation: {
        paddingTop: 0,
    },
    cardBodyDateLocation: {
        flexDirection: 'row',
        alignItems: 'center',
    },
    cardDateLocationText: {
        fontSize: 13,
        color: '#999',
    }
});

export default connect(
    mapStateToProps,
    { getAllDocketByWorkerId, getDocketDetailById }
)(DocketScreen);



