import React from 'react';
import {
    ActivityIndicator,
    AsyncStorage,
    StatusBar,
    StyleSheet,
    View,
} from 'react-native';
import { connect } from 'react-redux';

import { checkAuthFromAsyncStorage } from '../actions/authAction';

class AuthLoadingScreen extends React.Component {
    constructor(props) {
        super(props);
        // template
        // this._checkAuthFromAsyncStorage(); 
    }

    componentDidMount() {
        this._checkAuthFromAsyncStorage();
    }

    // template
    // // Fetch the token from storage then navigate to our appropriate place
    // _checkAuthFromAsyncStorage = async () => {
    //     const userToken = await AsyncStorage.getItem('userToken');

    //     // This will switch to the App screen or Auth screen and this loading
    //     // screen will be unmounted and thrown away.
    //     // this.props.navigation.navigate(userToken ? 'Main' : 'Login');
    //     this.props.navigation.navigate('Login');
    // };

    _checkAuthFromAsyncStorage = () => {
        const loginSuccessCallback = () => {
            this.props.navigation.navigate('Main');
        };

        const loginFailCallback = () => {
            this.props.navigation.navigate('Login');
        }

        this.props.checkAuthFromAsyncStorage(loginSuccessCallback, loginFailCallback);
    };

    // Render any loading content that you like here
    render() {
        return (
            <View style={styles.container}>
                <ActivityIndicator style={styles.loading} />
                <StatusBar barStyle="default" />
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fff',
    },
    loading: {
        flex: 1
    }
});

function mapStateToProps(state) {
    // console.log(state);
    return {
    };
}


export default connect(
    mapStateToProps,
    { checkAuthFromAsyncStorage },
)(AuthLoadingScreen);
