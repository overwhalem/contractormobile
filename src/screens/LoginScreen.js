import React from 'react';
import {
    StyleSheet,
    KeyboardAvoidingView,
    AsyncStorage
} from 'react-native';
import {
    Container,
    Button,
    Text,
    Form,
    Item,
    Input,
    Label,
} from 'native-base';
import { connect } from 'react-redux';

import { login } from '../actions/authAction';

class LoginScreen extends React.Component {
    static navigationOptions = {
        header: null,
    };

    constructor(props) {
        super(props);
        this.state = {
            username: "",
            password: "",
        };
    }

    render() {
        return (
            <Container style={styles.container}>
                <KeyboardAvoidingView behavior="padding" enabled style={styles.formContainer}>
                    <Form style={styles.loginForm}>

                        <Item floatingLabel>
                            <Label>Username</Label>
                            <Input
                                value={this.state.username}
                                onChangeText={(value) => this._onUserNameChange(value)}
                            />
                        </Item>
                        <Item floatingLabel last>
                            <Label>Password</Label>
                            <Input
                                secureTextEntry={true}
                                value={this.state.password}
                                onChangeText={(value) => this._onPasswordChange(value)}
                            />
                        </Item>

                        <Button full primary
                            onPress={this._onLoginPress}
                        >
                            <Text>Login</Text>
                        </Button>
                    </Form>
                </KeyboardAvoidingView>
            </Container >
        );
    }

    _onUserNameChange = (value) => {
        this.setState({
            username: value
        });
    };

    _onPasswordChange = (value) => {
        this.setState({
            password: value
        });
    }

    _onLoginPress = async () => {
        const { username, password } = this.state;

        const loginSuccessCallback = () => {
            this.props.navigation.navigate('Main');
        };

        this.props.login(username, password, loginSuccessCallback);
        // await AsyncStorage.setItem('userToken', 'abc');
        // this.props.navigation.navigate('Main');
    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    formContainer: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    },
    loginForm: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#fff',
    }
});

function mapStateToProps(state) {
    // console.log(state);
    return {
    };
}

export default connect(
    mapStateToProps,
    { login },
)(LoginScreen);