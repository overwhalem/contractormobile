import React, { Component } from 'react';
import { connect } from 'react-redux';

import {
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';
import { Camera, Permissions } from 'expo';
import { Container, Button, Text } from 'native-base';

import COLORS from '../constants/colors';
import { changePicture } from '../actions/docketFormAction';

class DocketFormCameraScreen extends Component {
    static navigationOptions = {
        // title: 'Envidences Capture',
        // headerStyle: {
        //     backgroundColor: COLORS.headerBackground,
        // },
        // headerTintColor: '#fff',
        // headerTitleStyle: {
        //     fontWeight: 'bold',
        // },
        header: null,
    };

    state = {
        hasCameraPermission: null,
        type: Camera.Constants.Type.back,
    };

    async componentDidMount() {
        const { status } = await Permissions.askAsync(Permissions.CAMERA);
        this.setState({ hasCameraPermission: status === 'granted' });
    }

    render() {
        const { hasCameraPermission } = this.state;
        if (hasCameraPermission === null) {
            return <Container />;
        } else if (hasCameraPermission === false) {
            return <Text>No access to camera</Text>;
        } else {
            return (
                <Container>
                    <Camera
                        style={{ flex: 1 }}
                        ref={ref => { this.camera = ref; }}
                        type={this.state.type}
                    >
                        <View
                            style={{
                                flex: 1,
                                backgroundColor: 'transparent',
                                flexDirection: 'row',
                            }}>
                            <View
                                style={{
                                    flex: 1,
                                    alignSelf: 'flex-end',
                                    justifyContent: 'center',
                                    alignItems: 'center',
                                    marginBottom: 10,
                                }}
                            >
                                <View>
                                    <Button
                                        bordered success
                                        onPress={this._capturePicture}
                                    >
                                        <Text>Capture</Text>
                                    </Button>
                                </View>
                            </View>
                        </View>
                    </Camera>
                </Container>
            );
        }
    }

    _capturePicture = async () => {
        if (this.camera) {
            let photo = await this.camera.takePictureAsync({
                quality: 1, // quality 0 for very poor 1 for very good
            });
            console.log(photo);
            this.props.changePicture(photo.uri);
            this.props.navigation.goBack();
        }
    }
}

function mapStateToProps(state) {
    return {
    };
}

const styles = StyleSheet.create({
    signatureCanvasContainer: {
        padding: 10,
        height: 400,
        backgroundColor: '#eee',
    },
    buttonContainer: {
        flex: 1,
        padding: 10,
        justifyContent: 'center',
        alignItems: 'center',
    },
    signatureCanvas: {
        flex: 1,
        backgroundColor: '#fff',
    },
});

export default connect(
    mapStateToProps,
    { changePicture },
)(DocketFormCameraScreen);