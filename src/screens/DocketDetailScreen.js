import React, { Component } from 'react';
import { connect } from 'react-redux';
import {
    ScrollView,
    StyleSheet,
    View,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    Button,
    Text,
    Card,
    CardItem
} from 'native-base';

import COLORS from '../constants/colors';
import { DOCKETSTATUSNEXTACTIONNAME, DOCKETSTATUSID } from '../constants/docketStatus';

import { setDocketStatusToWorkingById, setDocketStatusToNeedApproveById } from '../actions/docketDetailAction';

class DocketDetailScreen extends Component {
    static navigationOptions = {
        title: 'Chi tiết công việc',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    renderDocketInfo() {
        const { currentDocket } = this.props;
        return (
            <View style={styles.docketInfoContainer}>
                <Card style={styles.docketInfoCard}>
                    <Text>
                        Đây là thông tin chi tiết của docket.
                        Có data rồi. chưa có UI
                    </Text>
                </Card>
            </View>
        );
    };

    renderDocketForm() {
        return (
            <TouchableOpacity
                style={styles.docketFormContainer}
                onPress={() => this.props.navigation.navigate('DocketForm')}
            >
                <Card style={styles.docketFormCard}>
                    <Text>
                        Đây là cái form static sẽ được render.
                        Click vào chuyển sang update form.
                    </Text>
                </Card>
            </TouchableOpacity>
        );
    };

    renderChangeStatusButton() {
        const { currentDocket } = this.props;
        return (
            <View style={styles.changeStatusButtonContainer}>
                <Button
                    style={styles.changeStatusButton}
                    onPress={() => this._onChangeStatusButtonPress()}
                >
                    <Text>{DOCKETSTATUSNEXTACTIONNAME[currentDocket.status]}</Text>
                </Button>
            </View>
        );
    };

    render() {
        return (
            <Container style={styles.container}>
                <ScrollView style={styles.container} contentContainerStyle={styles.contentContainer}>
                    {this.renderDocketInfo()}

                    {this.renderDocketForm()}

                    {this.renderChangeStatusButton()}
                </ScrollView>
            </Container>
        );
    };

    _onChangeStatusButtonPress = () => {
        const { currentDocket } = this.props;

        const setSuccessCallback = () => {
            this.props.navigation.goBack();
        };

        switch (currentDocket.status) {
            case DOCKETSTATUSID.CONFIRMED:
                this.props.setDocketStatusToWorkingById(currentDocket.docketId, setSuccessCallback);
                break;
            case DOCKETSTATUSID.WORKING:
                this.props.setDocketStatusToNeedApproveById(currentDocket.docketId, setSuccessCallback);
                break;
            default:
                this.props.navigation.goBack();
                break;
        }


    };
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#fafafa',
    },
    contentContainer: {
        padding: 15,
        backgroundColor: '#fafafa'
    },
    docketInfoContainer: {
        height: 150,
    },
    docketInfoCard: {
        flex: 1,
    },

    docketFormContainer: {
        height: 600,
    },
    docketFormCard: {
        flex: 1,
    },

    changeStatusButtonContainer: {
        height: 50,
        flexDirection: 'row',
    },
    changeStatusButton: {
        flex: 1,
        backgroundColor: COLORS.tintColor,
        justifyContent: 'center',
        alignItems: 'center',
    }
});

function mapStateToProps(state) {
    return {
        currentDocket: state.docketDetailReducer.currentDocket
    };
}

export default connect(
    mapStateToProps,
    { setDocketStatusToWorkingById, setDocketStatusToNeedApproveById }
)(DocketDetailScreen);



