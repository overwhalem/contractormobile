import React, { Component } from 'react';
import {
    ScrollView,
    StyleSheet,
    View,
    Platform,
    TouchableOpacity
} from 'react-native';
import {
    Container,
    ListItem,
    Text,
    Icon,
    Left,
    Body,
    Right,
    Switch,
    Button
} from 'native-base';
import { connect } from 'react-redux';

import { logout } from '../actions/authAction';


import COLORS from '../constants/colors';

class SettingScreen extends Component {
    static navigationOptions = {
        title: 'Tùy chọn',
        headerStyle: {
            backgroundColor: COLORS.headerBackground,
        },
        headerTintColor: '#fff',
        headerTitleStyle: {
            fontWeight: 'bold',
        },
    };

    render() {
        return (
            <Container style={styles.container}>
                <ScrollView>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#FF9501" }}>
                                <Icon active name="airplane" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Airplane Mode</Text>
                        </Body>
                        <Right>
                            <Switch value={false} />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#007AFF" }}>
                                <Icon active name="wifi" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>Wi-Fi</Text>
                        </Body>
                        <Right>
                            <Text>GeekyAnts</Text>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button style={{ backgroundColor: "#007AFF" }}>
                                <Icon active name="md-done-all" />
                            </Button>
                        </Left>
                        <Body>
                            <Text>History</Text>
                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                    <ListItem icon>
                        <Left>
                            <Button
                                style={{ backgroundColor: "#007AFF" }}
                                onPress={this._onLogoutClick}
                            >
                                <Icon active
                                    name={Platform.OS === 'ios'
                                        ? 'ios-log-out'
                                        : 'md-log-out'}
                                />
                            </Button>
                        </Left>
                        <Body>
                            <TouchableOpacity
                                onPress={this._onLogoutClick}
                            >
                                <Text>Sign out</Text>
                            </TouchableOpacity>

                        </Body>
                        <Right>
                            <Icon active name="arrow-forward" />
                        </Right>
                    </ListItem>
                </ScrollView>
            </Container>
        );
    };

    _onLogoutClick = () => {
        const logoutSuccessCallback = () => {
            this.props.navigation.navigate('Login');
        }

        this.props.logout(logoutSuccessCallback);
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginTop: 15,
        backgroundColor: '#fff',
    },
    contentContainer: {
        paddingTop: 30,
    },
    welcomeContainer: {
        alignItems: 'center',
        marginTop: 10,
        marginBottom: 20,
    },
    codeHighlightText: {
        color: 'rgba(96,100,109, 0.8)',
    },
    codeHighlightContainer: {
        backgroundColor: 'rgba(0,0,0,0.05)',
        borderRadius: 3,
        paddingHorizontal: 4,
    },
});

function mapStateToProps(state) {
    return {
    };
}

export default connect(
    mapStateToProps,
    { logout }
)(SettingScreen);



