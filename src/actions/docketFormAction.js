import { CHANGE_PICTURE, CHANGE_SIGNATURE } from "./types";


export const changePicture = (pictureUrl) => {
    return {
        type: CHANGE_PICTURE,
        pictureUrl
    };
};

export const changeSignature = (signatureUrl) => {
    return {
        type: CHANGE_SIGNATURE,
        signatureUrl
    };
};