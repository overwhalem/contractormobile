import {
    GET_ALL_DOCKET_BY_WORKER_ID_SUCCESS,
    GET_ALL_DOCKET_BY_WORKER_ID_LOADING,
    GET_ALL_DOCKET_BY_WORKER_ID_FAIL,
} from "./types";
import { apiGetAllDocketByWorkerId } from "../apis/docketApi";


export const getAllDocketByWorkerId = (workerId, callback) => {
    return async (dispatch) => {
        // var myHeaders = new Headers();
        // myHeaders.set('Accept', 'application/json');
        // myHeaders.set('Content-Type', 'application/json');
        // myHeaders.set('Cache-Control', 'no-cache');
        // myHeaders.set('Pragma', 'no-cache');
        // myHeaders.set('Expires', '0');

        // try {
        //     let response = await fetch('http://192.168.42.1:8090/api/workers/152/dockets?random=454545545', {
        //         method: 'GET',
        //         headers: {
        //             'Cache-Control': 'no-cache, no-store, must-revalidate',
        //             'Pragma': 'no-cache',
        //             'Expires': 0
        //         },
        //     });
        //     let responseJson = await response.json();

        //     console.log(response);
        //     // return responseJson.movies;
        // } catch (error) {
        //     console.error(error);
        // }
        dispatch({
            type: GET_ALL_DOCKET_BY_WORKER_ID_LOADING
        });

        try {
            const response = await apiGetAllDocketByWorkerId(workerId);

            if (response && response.data) {
                const data = response.data;
                if (data) {

                    dispatch({
                        type: GET_ALL_DOCKET_BY_WORKER_ID_SUCCESS,
                        docketList: data,
                    });

                } else {
                    dispatch({
                        type: GET_ALL_DOCKET_BY_WORKER_ID_FAIL
                    });
                }
            }
        } catch (error) {
            alert(error.message);
        }

    };
};