import {
    GET_DOCKET_DETAIL_SUCCESS,
    GET_DOCKET_DETAIL_LOADING,
    GET_DOCKET_DETAIL_FAIL,
    SET_DOCKET_STATUS_LOADING,
    SET_DOCKET_STATUS_SUCCESS,
    SET_DOCKET_STATUS_FAIL,
} from "./types";

import { apiGetDocketDetailById, apiSetDocketStatusToWorkingById, apiSetDocketStatusToNeedApproveById } from "../apis/docketApi";

export const getDocketDetailById = (docketId, getSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: GET_DOCKET_DETAIL_LOADING
        });

        try {
            const response = await apiGetDocketDetailById(docketId);

            if (response && response.data) {
                const data = response.data;
                if (data) {

                    dispatch({
                        type: GET_DOCKET_DETAIL_SUCCESS,
                        currentDocket: data,
                    });

                    if (getSuccessCallback) {
                        getSuccessCallback();
                    }

                } else {
                    dispatch({
                        type: GET_DOCKET_DETAIL_FAIL
                    });
                }
            }
        } catch (error) {
            alert(error.message);
        }

    };
};


export const setDocketStatusToWorkingById = (docketId, setSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SET_DOCKET_STATUS_LOADING
        });

        try {
            const response = await apiSetDocketStatusToWorkingById(docketId);

            if (response && response.data) {
                const data = response.data;
                if (data && data.message === "OK") {

                    dispatch({
                        type: SET_DOCKET_STATUS_SUCCESS,
                    });

                    if (setSuccessCallback) {
                        setSuccessCallback();
                    }

                } else {
                    dispatch({
                        type: SET_DOCKET_STATUS_FAIL
                    });
                }
            }
        } catch (error) {
            alert(error.message);
        }

    };
};

export const setDocketStatusToNeedApproveById = (docketId, setSuccessCallback) => {
    return async (dispatch) => {
        dispatch({
            type: SET_DOCKET_STATUS_LOADING
        });

        try {
            const response = await apiSetDocketStatusToNeedApproveById(docketId);

            if (response && response.data) {
                const data = response.data;
                if (data && data.message === "OK") {

                    dispatch({
                        type: SET_DOCKET_STATUS_SUCCESS,
                    });

                    if (setSuccessCallback) {
                        setSuccessCallback();
                    }

                } else {
                    dispatch({
                        type: SET_DOCKET_STATUS_FAIL
                    });
                }
            }
        } catch (error) {
            alert(error.message);
        }

    };
};