import {
    LOGIN_LOADING,
    LOGIN_SUCCESS,
    LOGIN_FAIL,
    LOGOUT_SUCCESS,
} from "./types";
import { apiLogin } from "../apis/authApi";

import {
    AsyncStorage
} from 'react-native';

import { ASYNC_STORAGE_KEY } from '../constants/asyncStorage';

export const login = (username, password, loginSuccessCallback, loginFailCallback) => {
    return async (dispatch) => {
        dispatch({
            type: LOGIN_LOADING
        });

        let response
        try {
            response = await apiLogin(username, password);
        } catch (error) {
            alert(error.message);
        }

        if (response && response.data) {
            const data = response.data;
            if (data && data.accountId) {

                try {
                    await AsyncStorage.setItem(ASYNC_STORAGE_KEY.AUTH_PROFILE, JSON.stringify(data));
                } catch (error) {
                    // Error saving data
                }

                dispatch({
                    type: LOGIN_SUCCESS,
                    profile: data
                });

                if (loginSuccessCallback) {
                    loginSuccessCallback();
                }
            } else {
                dispatch({
                    type: LOGIN_FAIL
                });

                if (loginFailCallback) {
                    loginFailCallback();
                }
            }
        }

    };
};

export const checkAuthFromAsyncStorage = (loginSuccessCallback, loginFailCallback) => {
    return async (dispatch) => {
        let profileString
        try {
            profileString = await AsyncStorage.getItem(ASYNC_STORAGE_KEY.AUTH_PROFILE);
        } catch (error) {
            // Error get data
        };

        if (profileString) {
            const profile = JSON.parse(profileString);
            if (profile && profile.accountId) {
                dispatch({
                    type: LOGIN_SUCCESS,
                    profile
                });

                if (loginSuccessCallback) {
                    loginSuccessCallback();
                }

            } else {
                try {
                    await AsyncStorage.removeItem(ASYNC_STORAGE_KEY.AUTH_PROFILE);
                } catch (error) {
                    // Error remove data
                }

                if (loginFailCallback) {
                    loginFailCallback();
                }
            }
        } else {
            if (loginFailCallback) {
                loginFailCallback();
            }
        }

    }
}

export const logout = (logoutSuccessCallback) => {
    return async (dispatch) => {

        try {
            await AsyncStorage.removeItem(ASYNC_STORAGE_KEY.AUTH_PROFILE);
        } catch (error) {
            // Error remove data
        }

        dispatch({
            type: LOGOUT_SUCCESS
        });

        if (logoutSuccessCallback) {
            logoutSuccessCallback();
        }
    }
};