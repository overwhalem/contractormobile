export const DOCKETSTATUSNAME = [
    "INIT",
    "FILLED",
    "Mới",
    "DECLINED",
    "Đang làm",
    "Chờ duyệt",
    "Hoàn tất"
]

export const DOCKETSTATUSID = {
    INIT: 0,
    FILLED: 1,
    CONFIRMED: 2,
    DECLINED: 3,
    WORKING: 4,
    NEEDAPPROVE: 5,
    APPROVED: 6,
}

export const DOCKETSTATUSNEXTACTIONNAME = [
    "INIT",
    "FILLED",
    "Bắt đầu làm việc",
    "DECLINED",
    "Hoàn tất công việc",
    "Chờ xác nhận",
    "Hoàn tất"
]