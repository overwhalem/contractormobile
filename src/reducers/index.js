import { combineReducers } from "redux";

import docketFormReducer from './docketFormReducer';
import authReducer from './authReducer';
import docketReducer from './docketReducer';
import docketDetailReducer from './docketDetailReducer';


export default combineReducers({
    authReducer,
    docketFormReducer,
    docketReducer,
    docketDetailReducer
});