import { CHANGE_PICTURE, CHANGE_SIGNATURE } from "../actions/types";

const INIT_STATE = {
    pictureUrl: null,
    signatureUrl: null,
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case CHANGE_PICTURE:
            return {
                ...state,
                pictureUrl: action.pictureUrl,
            };
        case CHANGE_SIGNATURE:
            return {
                ...state,
                signatureUrl: action.signatureUrl,
            };
        default:
            return state;
    }
};