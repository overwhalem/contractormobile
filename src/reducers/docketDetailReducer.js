import {
    GET_DOCKET_DETAIL_LOADING,
    GET_DOCKET_DETAIL_SUCCESS,
    GET_DOCKET_DETAIL_FAIL,
    SET_DOCKET_STATUS_FAIL,
    SET_DOCKET_STATUS_SUCCESS,
    SET_DOCKET_STATUS_LOADING
} from "../actions/types";

const INIT_STATE = {
    isFetching: false,
    currentDocket: {},
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_DOCKET_DETAIL_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case GET_DOCKET_DETAIL_SUCCESS:
            return {
                isFetching: false,
                currentDocket: action.currentDocket,
            };
        case GET_DOCKET_DETAIL_FAIL:
            return {
                isFetching: false,
                currentDocket: {}
            };
        case SET_DOCKET_STATUS_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case SET_DOCKET_STATUS_SUCCESS:
            return {
                ...state,
                isFetching: false,
            };
        case SET_DOCKET_STATUS_FAIL:
            return {
                ...state,
                isFetching: false,
            };
        default:
            return state;
    }
};