import { GET_ALL_DOCKET_BY_WORKER_ID_FAIL, GET_ALL_DOCKET_BY_WORKER_ID_SUCCESS, GET_ALL_DOCKET_BY_WORKER_ID_LOADING } from "../actions/types";

const INIT_STATE = {
    isFetching: false,
    docketList: [],
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case GET_ALL_DOCKET_BY_WORKER_ID_LOADING:
            return {
                ...state,
                isFetching: true,
            };
        case GET_ALL_DOCKET_BY_WORKER_ID_SUCCESS:
            return {
                isFetching: false,
                docketList: action.docketList,
            };
        case GET_ALL_DOCKET_BY_WORKER_ID_FAIL:
            return {
                isFetching: false,
                docketList: []
            };
        default:
            return state;
    }
};