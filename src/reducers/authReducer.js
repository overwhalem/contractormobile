import { LOGIN_LOADING, LOGIN_SUCCESS, LOGIN_FAIL, LOGOUT_SUCCESS } from "../actions/types";

const INIT_STATE = {
    isFetching: false,
    isLogged: false,
    profile: {
    }
};

export default (state = INIT_STATE, action) => {
    switch (action.type) {
        case LOGIN_LOADING:
            return {
                isFetching: true,
                isLogged: false,
                profile: {}
            };
        case LOGIN_SUCCESS:
            return {
                isFetching: false,
                isLogged: true,
                profile: action.profile
            };
        case LOGIN_FAIL:
            return {
                isFetching: false,
                isLogged: false,
                profile: {}
            };
        case LOGOUT_SUCCESS:
            return INIT_STATE;
        default:
            return state;
    }
};